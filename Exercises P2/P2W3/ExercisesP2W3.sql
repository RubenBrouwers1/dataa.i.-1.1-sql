--If you remove a view, the underlying table will not be affected
DROP VIEW view_d_songs;
DROP VIEW view_salary_dep_man;
DROP TABLE COPY_D_CDS;
DROP VIEW read_copy_d_cds;
DROP VIEW view_ex4;
DROP VIEW view_ex4_r;

--Ex1

SELECT * FROM D_SONGS;

CREATE VIEW view_d_songs
AS
    SELECT ID, TITLE AS "Song Title", ARTIST
    FROM D_SONGS s
         JOIN D_TYPES t ON s.type_code = t.code
    WHERE lower(t.description) LIKE '%new age%'
;

CREATE OR REPLACE VIEW  view_d_songs AS
    SELECT ID AS "Song ID", TITLE AS "Song Title", ARTIST AS "Artist", TYPE_CODE || ': ' || DESCRIPTION AS "Type code"
    FROM D_SONGS s
             JOIN D_TYPES t ON s.type_code = t.code
    WHERE lower(t.description) LIKE '%new age%'
;

SELECT * FROM view_d_songs;

--EX2

CREATE OR REPLACE VIEW view_salary_dep_man
AS
    SELECT D.DEPARTMENT_NAME AS "Department Name",
           MIN(E.SALARY) AS "Minimum Salary",
           MAX(E.SALARY) AS "Maximum Salary",
           ROUND(AVG(E.SALARY),2) AS "Average Salary"
    FROM EMPLOYEES E LEFT JOIN DEPARTMENTS D on E.DEPARTMENT_ID = D.DEPARTMENT_ID
    GROUP BY DEPARTMENT_NAME
    --It is best that you do not put this order by clause here, but rather when you retrieve the data from the view. For performance reasons
    ORDER BY DEPARTMENT_NAME NULLS LAST;

--You could also create the aliases like 'CREATE OR REPLACE VIEW view_salary_dep_man ("Department Name", "Minimum Salary", "Maximum Salary", "Average Salary")'

SELECT * FROM view_salary_dep_man;

--EX3
--You can remove a row, modify data or add data to the table through the view, if the 'create view' statement has/hasn't got certain elements

CREATE TABLE COPY_D_CDS
AS SELECT * FROM D_CDS;

CREATE OR REPLACE VIEW read_copy_d_cds
AS
    SELECT *
    FROM COPY_D_CDS
    WHERE YEAR = 2000
WITH READ ONLY;
--This makes sure that the user of that view can not harm the data in any way

SELECT * FROM read_copy_d_cds;

--B. This can not work, you can only view the view right now, if the 'WITH READ ONLY' is gone, you can also modify within the domain of the view

--C.
CREATE OR REPLACE VIEW read_copy_d_cds
AS
    SELECT *
    FROM COPY_D_CDS
    WHERE YEAR = '2000'
WITH CHECK OPTION CONSTRAINT read_copy_d_cds;

--D.
DELETE FROM READ_COPY_D_CDS
WHERE CD_NUMBER = 91;

--EX4

-- I struggled with the comma after the first table. I was unsure if we could use an inline stuff as a second table, I would have personally also used a left join

CREATE OR REPLACE VIEW view_ex4
AS
    SELECT E.LAST_NAME, E.SALARY, E.DEPARTMENT_ID, D.maxsal
    FROM EMPLOYEES E,
         (SELECT DEPARTMENT_ID, max(SALARY) maxsal
          FROM EMPLOYEES
          GROUP BY DEPARTMENT_ID) D
    WHERE E.DEPARTMENT_ID = D.DEPARTMENT_ID AND e.SALARY = D.maxsal;

CREATE OR REPLACE VIEW view_ex4_r
AS
SELECT E.LAST_NAME, E.SALARY, E.DEPARTMENT_ID, D.maxsal
FROM EMPLOYEES E LEFT JOIN
     (SELECT DEPARTMENT_ID, max(SALARY) maxsal
      FROM EMPLOYEES
      GROUP BY DEPARTMENT_ID) D
ON E.DEPARTMENT_ID = D.DEPARTMENT_ID AND e.SALARY = D.maxsal;

SELECT * FROM view_ex4;
SELECT * FROM view_ex4_r;


--ROWNUM
--if you only want to take the first 5 results
--'SELECT ROWNUM AS 'Longest employed', LAST_NAME, HIRE_DATE
--'FROM (SELECT LAST_NAME, HIRE_DATE
--'      FROM EMPLOYEES
--'      ORDER BY HIRE_DATE (DESC)
--'WHERE ROWNUM <=5;
DROP TABLE my_departments PURGE;
DROP VIEW view_my_dep;

--EX1
CREATE TABLE my_departments
AS (SELECT * FROM DEPARTMENTS);

SELECT * FROM my_departments;

--EX2
SELECT *
FROM USER_CONS_COLUMNS
WHERE lower(TABLE_NAME) = 'my_departments';

ALTER TABLE my_departments DISABLE CONSTRAINT SYS_C0021883;

--EX3
CREATE OR REPLACE VIEW view_my_dep
AS SELECT DEPARTMENT_ID, DEPARTMENT_NAME
    FROM my_departments;

select * from view_my_dep;

--EX4
INSERT INTO view_my_dep VALUES (105, 'Advertising');
INSERT INTO view_my_dep VALUES (210, 'Custodial');
INSERT INTO view_my_dep VALUES (130, 'Planning');

--EX5
    --Outline constraint (more suitable if you have multiple columns)
ALTER TABLE my_departments
ADD CONSTRAINT my_dep_id_pk PRIMARY KEY (DEPARTMENT_ID);
    --Inline constraint (if you only want to change one column)
ALTER TABLE my_departments
MODIFY department_id CONSTRAINT my_dep_id_pk PRIMARY KEY;

--EX6
INSERT INTO view_my_dep (department_name) VALUES ('Human Resources');

--EX7
INSERT INTO view_my_dep VALUES (220, 'Human Resources');

--EX9
CREATE OR REPLACE VIEW view_my_dep
AS SELECT DEPARTMENT_ID, DEPARTMENT_NAME, LOCATION_ID
    FROM MY_DEPARTMENTS;

--EX10
ALTER TABLE my_departments
MODIFY location_id CONSTRAINT my_dep_loc_id_nn NOT NULL;

SELECT * FROM MY_DEPARTMENTS;

UPDATE my_departments
SET (MANAGER_ID) = (SELECT MANAGER_ID
        FROM MY_DEPARTMENTS
        WHERE DEPARTMENT_ID = 10)
WHERE MANAGER_ID IS NULL;

UPDATE my_departments
SET (LOCATION_ID) = (SELECT LOCATION_ID
                    FROM MY_DEPARTMENTS
                    WHERE DEPARTMENT_ID = 10)
WHERE LOCATION_ID IS NULL;
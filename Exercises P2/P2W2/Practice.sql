--EX2
CREATE TABLE my_student
(ID NUMBER(6)
    CONSTRAINT my_stud_ID_pk PRIMARY KEY,
firstn VARCHAR2(10),
lastn VARCHAR2(10)
);

CREATE TABLE my_subject
(ID NUMBER(6)
    CONSTRAINT my_sub_id_pk PRIMARY KEY,
Name VARCHAR2(10)
);

CREATE TABLE my_enrollment
(Grade NUMBER(2),
Student_id NUMBER(6)
    CONSTRAINT my_enr_stud_id_fk REFERENCES my_student(ID),
Subject_id NUMBER(6)
    CONSTRAINT my_enr_subj_id_fk REFERENCES my_subject(ID),
 CONSTRAINT enrollments_pk PRIMARY KEY (student_id, subject_id)
);
DROP TABLE store_locations PURGE;
DROP TABLE my_enrollments PURGE;
DROP TABLE my_subject PURGE;
DROP TABLE my_students PURGE;
DROP TABLE COPY_D_SONGS PURGE;
DROP SEQUENCE SEQ_D_SONGS_SEQ;
DROP TABLE COPY_D_TRACK_LISTINGS PURGE;
DROP TABLE copy_departments PURGE;
DROP INDEX cd_number_ix;
DROP SYNONYM my_tracks;


--EX1

CREATE TABLE store_locations
(id NUMBER(4)
    CONSTRAINT store_loc_id_pk PRIMARY KEY ,
name VARCHAR2(15),
date_opened DATE DEFAULT SYSDATE
    CONSTRAINT store_loc_odate__nn NOT NULL,
address VARCHAR2(30)
    CONSTRAINT store_loc_address__nn NOT NULL,
city VARCHAR2(30)
     CONSTRAINT store_loc_city__nn NOT NULL,
zip_code number(4)
     CONSTRAINT store_loc_Zcode__nn NOT NULL,
phone VARCHAR2(20),
email VARCHAR2(40)
    CONSTRAINT store_loc_email_uk UNIQUE ,
manager_id NUMBER(5),
Emergency_contact VARCHAR2(40));

--EX2

CREATE TABLE my_enrollments
(
    grade VARCHAR2(2),
    student_id NUMBER(6)
        CONSTRAINT my_enr_st_id_fk REFERENCES my_students(id),
    subject_id NUMBER(6)
        CONSTRAINT my_enr_sub_id_fk REFERENCES my_subject(id),
    CONSTRAINT enrollments_pk PRIMARY KEY (student_id, subject_id)
);

CREATE TABLE my_students
(
    ID NUMBER(3)
        CONSTRAINT my_stud_id_pk PRIMARY KEY,
    fname VARCHAR2(30),
    lname VARCHAR2(30)
        CONSTRAINT my_stud_lname_nn NOT NULL
);

CREATE TABLE my_subject
(
    ID NUMBER(3)
        CONSTRAINT my_sub_id_pk PRIMARY KEY,
    name VARCHAR2(30)
);

--EX3

CREATE TABLE copy_d_songs
AS (SELECT * FROM D_SONGS);

CREATE SEQUENCE SEQ_D_SONGS_SEQ
    START WITH 100
    MAXVALUE 1000
    INCREMENT BY 2
    NOCACHE
    NOCYCLE;

INSERT INTO COPY_D_SONGS (ID, TITLE, DURATION, ARTIST, TYPE_CODE)
VALUES (SEQ_D_SONGS_SEQ.nextval, 'Island Fever', '5 min', 'Hawaiian Islanders', 12);

INSERT INTO COPY_D_SONGS (ID, TITLE, DURATION, ARTIST, TYPE_CODE)
VALUES (SEQ_D_SONGS_SEQ.nextval, 'Castle of Dreams', '4 min', 'The Wanderers', 77);

--EX4

CREATE TABLE copy_d_track_listings
AS (SELECT * FROM D_TRACK_LISTINGS);

CREATE INDEX cd_number_ix
ON copy_d_track_listings (CD_NUMBER);

SELECT * FROM USER_INDEXES WHERE INDEX_NAME LIKE '%NAME%';

CREATE SYNONYM my_tracks
    FOR copy_d_track_listings;

--EX5

CREATE TABLE copy_departments
AS (SELECT * FROM DEPARTMENTS);

--Inline constraint
ALTER TABLE copy_departments
    MODIFY DEPARTMENT_ID CONSTRAINT copy_dept_id_pk PRIMARY KEY;

--Outline constraint
ALTER TABLE copy_departments
    ADD CONSTRAINT  copy_dept_lock_fk FOREIGN KEY(LOCATION_ID)  REFERENCES LOCATIONS
    ADD CONSTRAINT copy_dept_mgr_fk FOREIGN KEY (MANAGER_ID) REFERENCES EMPLOYEES
    ADD CONSTRAINT copy_dept_id_pk PRIMARY KEY (DEPARTMENT_ID) ENABLE;

--Not null can not be an outline constraint
ALTER TABLE copy_departments
    MODIFY DEPARTMENT_ID CONSTRAINT copy_dept_mangr_name NOT NULL;

SELECT * FROM USER_TAB_COLUMNS WHERE TABLE_NAME = 'COPY_DEPARTMENTS';
SELECT * FROM USER_CONSTRAINTS WHERE TABLE_NAME = 'COPY_DEPARTMENTS';

CREATE INDEX copy_departments_locix ON copy_departments(LOCATION_ID);

--EX6

GRANT READ ON copy_departments TO PUBLIC;

GRANT SELECT ON copy_departments TO Smith;

--EX7

CREATE TABLE my_books
(
    ISBN NUMBER(13)
        CONSTRAINT my_book_isbn_pk PRIMARY KEY,
    Title VARCHAR2(30),
    Author VARCHAR2(30)
);

CREATE TABLE my_copies
(
    ID_number NUMBER(8)
        CONSTRAINT cop_book_number_pk PRIMARY KEY,
    ISBN NUMBER(13)
        CONSTRAINT copies_book_ISBN_fk REFERENCES my_books(ISBN)
);

CREATE TABLE my_borrowers
(
    ID_number VARCHAR2(15) CONSTRAINT id_number_pk PRIMARY KEY,
    Name VARCHAR2(30)
        CONSTRAINT copy_book_number_pk PRIMARY KEY ,
    Position VARCHAR(30)
);

--There is something wrong with these references
CREATE TABLE  my_book_loans
(
book_number NUMBER(8) CONSTRAINT book_loans_copy_booknum_fk REFERENCES my_copies(ID_number),
borrower_id VARCHAR2(30) CONSTRAINT book_loans_bor_id_numb_fk REFERENCES my_borrowers(ID_number),
date_borrowed DATE,
date_returned DATE,
CONSTRAINT borrower_pk PRIMARY KEY (book_number, borrower_id, date_borrowed,)
);

--Extra EX11
DROP VIEW complex_view;

SELECT * FROM LOCATIONS;

CREATE OR REPLACE VIEW complex_view
AS(
SELECT d.DEPARTMENT_NAME, l.STREET_ADDRESS, l.CITY, l.STATE_PROVINCE
FROM LOCATIONS l LEFT JOIN DEPARTMENTS d  ON l.LOCATION_ID = d.LOCATION_ID
WHERE COUNTRY_ID = 'US');

SELECT * FROM complex_view;
DROP TABLE COPY_LOCATIONS;

--EX1
--A
CREATE ROLE manager;

GRANT SELECT, INSERT, UPDATE, DELETE
    ON EMPLOYEES
    TO manager;

--B
CREATE ROLE clerk;

GRANT SELECT, INSERT
ON EMPLOYEES
TO clerk;

--C
GRANT manager TO scott;

--D
REVOKE DELETE
ON EMPLOYEES
FROM manager;

--EX2
--A
SELECT * FROM LOCATIONS;

--B
CREATE TABLE copy_locations
AS SELECT * FROM LOCATIONS;

SELECT REGEXP_REPLACE(STREET_ADDRESS, ' ', '')
FROM COPY_LOCATIONS;

--EX3
--A
SELECT FIRST_NAME, LAST_NAME
FROM EMPLOYEES
WHERE REGEXP_COUNT(first_name, ('.')) = 7;


--B
SELECT COUNT(PHONE_NUMBER)
FROM EMPLOYEES
WHERE REGEXP_COUNT(PHONE_NUMBER, '\d{3}\.\d{3}\.\d{4}') = 0;

--C
ALTER TABLE EMPLOYEES
MODIFY phone_number CONSTRAINT phone_number_rule CHECK (REGEXP_LIKE(phone_number, '\d{3}\.\d{3}\.\d{4}'));

--EX4
--Isn't it ROLLBACK TO SAVEPOINT my_savepoint1?
--So all changes will be committed, because the system did not know to what savepoint to roll back to?

--EX5
INSERT INTO COUNTRIES
VALUES('BE', 'Belgium', 1);

INSERT INTO LOCATIONS (LOCATION_ID, STREET_ADDRESS, POSTAL_CODE, CITY, STATE_PROVINCE, COUNTRY_ID)
VALUES (0001, 'Nationalestraat 5', '2000', 'Antwerp','Antwerp', 'BE');

UPDATE DEPARTMENTS
SET LOCATION_ID = 0001
WHERE LOCATION_ID = (SELECT LOCATION_ID
                     FROM LOCATIONS
                     WHERE COUNTRY_ID = 'UK');

SELECT SYSDATE FROM DUAL;
--Look at the powerpoint for the multiple choice questions
--1 B
--2 D


--EX3
Select d.Department_name, d.Manager_id,e.last_name AS "Manager_Name", round(avg(sal.Salary)) AS AVG_DEPT_SALARY
FROM Departments d JOIN Employees e on(e.Employee_ID =d.manager_id)
                   Join Employees sal on(d.department_ID=sal.Department_id)
group by Department_name, e.LAST_NAME,d.MANAGER_ID;

--EX4
SELECT e.LAST_NAME, e.SALARY, e.DEPARTMENT_ID, AVGSal
FROM EMPLOYEES e
JOIN (
    SELECT TRUNC(AVG(SALARY)) AVGSal, DEPARTMENT_ID
    FROM EMPLOYEES
    GROUP BY DEPARTMENT_ID
    ) department_idAVG ON department_idAVG.DEPARTMENT_ID = E.DEPARTMENT_ID
WHERE SALARY > AVGSal;

--EX5
CREATE OR REPLACE VIEW view_avg_dep_sal AS
    SELECT d.DEPARTMENT_ID, d.MANAGER_ID, e.LAST_NAME man_name, round(avg(sal.SALARY)) AS AVG_DEP_SAL
    FROM DEPARTMENTS d
            JOIN EMPLOYEES E on d.MANAGER_ID = e.EMPLOYEE_ID
            JOIN EMPLOYEES sal on d.DEPARTMENT_ID = sal.DEPARTMENT_ID
    GROUP BY d.DEPARTMENT_ID, e.LAST_NAME, d.MANAGER_ID;

--EX6
CREATE OR REPLACE VIEW V2 AS
    SELECT DEPARTMENT_NAME, MIN(SALARY) AS "Lowest salary", MAX(SALARY) as "Highest Salary", ROUND(AVG(SALARY), 0) AS "Average Salary"
    FROM DEPARTMENTS d join employees e on d.DEPARTMENT_ID = e.DEPARTMENT_ID
    GROUP BY DEPARTMENT_NAME;

SELECT * FROM V2;


CREATE TABLE time_ex4
(loan_duration1 INTERVAL YEAR(3) TO MONTH,
 loan_duration2 INTERVAL YEAR(2) TO MONTH);

INSERT INTO time_ex4 (loan_duration1,
                      loan_duration2)
VALUES (INTERVAL '120' MONTH(3),
        INTERVAL '3-6' YEAR TO MONTH);

SELECT SYSDATE + loan_duration1 AS "120 m",
       SYSDATE + loan_duration2 AS "3 y + 6 m"

FROM time_ex4;

DROP TABLE TIME_EX4 PURGE ;
--It is handy to make everything rerunnable by first dropping all the tables made in this file
DROP TABlE copy_employees PURGE;
DROP TABlE copy_f_staffs PURGE;
DROP TABlE copy_f_promotional_menus PURGE;
DROP TABlE copy_d_clients PURGE;
DROP TABlE rep_email PURGE;
DROP TABlE time_ex4 PURGE;
DROP TABlE TIME_EX4 PURGE;

--EX0

CREATE TABLE copy_d_clients
AS (SELECT * FROM D_CLIENTS);

CREATE TABLE copy_f_staffs
AS (SELECT * FROM F_STAFFS);

CREATE TABLE copy_employees
AS (SELECT * FROM EMPLOYEES);

--EX1

INSERT INTO copy_d_clients
    (Client_Number, First_Name, Last_Name, Phone, Email)
    VALUES (6655, 'Ayako', 'Dahish', 3608859030, 'dahisha@harbor.net');

INSERT INTO copy_d_clients
(Client_Number, First_Name, Last_Name, Phone, Email)
VALUES (6689, 'Nick', 'Neuville', 9048953049, 'nnicky@charter.net');

--EX2

CREATE TABLE rep_email
(id NUMBER(3) CONSTRAINT rel_id_pk PRIMARY KEY,
 first_name VARCHAR2(10),
 last_name VARCHAR2(10),
 email_address VARCHAR2(10));

INSERT INTO rep_email
(id, first_name, last_name, email_address)
(SELECT EMPLOYEE_ID, first_name, last_name, email
        FROM EMPLOYEES
        WHERE JOB_ID LIKE '%REP%');

--EX3

SELECT *
FROM COPY_F_STAFFS;

UPDATE copy_f_staffs
SET OVERTIME_RATE = NVL(OVERTIME_RATE, 0) + 0.75
WHERE ID = (SELECT ID
            FROM COPY_F_STAFFS
            WHERE FIRST_NAME LIKE '%Bob%' AND
                    LAST_NAME LIKE '%Miller%');

UPDATE copy_f_staffs
SET OVERTIME_RATE = OVERTIME_RATE + 0.85
WHERE ID = (SELECT ID
            FROM COPY_F_STAFFS
            WHERE FIRST_NAME LIKE '%Sue%' AND
                    LAST_NAME LIKE '%Doe%');

--EX4

INSERT INTO copy_f_staffs (ID, FIRST_NAME, LAST_NAME, BIRTHDATE, SALARY, STAFF_TYPE)
VALUES (25, 'Kai', 'Kim', '03-NOV-1988', 6.75, 'Order Taker');

UPDATE copy_f_staffs
SET STAFF_TYPE = 'Waiter'
WHERE STAFF_TYPE = 'Order Taker';

--EX5

CREATE TABLE copy_f_promotional_menus
AS (SELECT * FROM F_PROMOTIONAL_MENUS);

ALTER TABLE copy_f_promotional_menus
    MODIFY(start_date DATE DEFAULT SYSDATE);

SELECT * FROM copy_f_promotional_menus;

INSERT INTO copy_f_promotional_menus (CODE, NAME, START_DATE, END_DATE, GIVE_AWAY)
VALUES (120, 'New Costumer', DEFAULT, '01-Jun-2005', '10% Discount coupon');

--EX6

CREATE TABLE my_ruben
AS SELECT CLIENT_NUMBER, FIRST_NAME, EMAIL FROM D_CLIENTS;

SELECT * FROM my_ruben;

INSERT INTO my_ruben
VALUES (0001, 'Ruben', 'ruben.brouwers1@gmail.com');

ALTER TABLE my_ruben
MODIFY EMAIL CONSTRAINT my_rub_email_chk CHECK (REGEXP_LIKE(EMAIL, '.+@.+\..+'));

INSERT INTO my_ruben
VALUES (2, 'Ann', 'rubeniscool');
;

--EX7

SELECT table_name
FROM user_tables;

--EX8

CREATE TABLE time_ex4
(exact_time TIMESTAMP);

SELECT * FROM time_ex4;

INSERT INTO time_ex4
VALUES ('18-Nov_2020 11:50:20');

--There is something wrong with this to-date translation
INSERT INTO time_ex4
VALUES (TO_DATE('18-Nov-2020 11:50:20.123456', 'DD-Mon-YYYY HH:MI:SS,FF6'));

INSERT INTO time_ex4
VALUES (TO_DATE('18/11/2020', 'DD/MM/YYYY'));

--EX9

SELECT * FROM copy_employees;

ALTER TABLE copy_employees
ADD start_date TIMESTAMP;

ALTER TABLE copy_employees
MODIFY (last_name VARCHAR2(35));

ALTER TABLE copy_employees
    MODIFY (start_date TIMESTAMP DEFAULT SYSTIMESTAMP);

UPDATE copy_employees
SET START_DATE = DEFAULT
WHERE EMPLOYEE_ID = 102;

ALTER TABLE copy_employees
DROP COLUMN START_DATE;
--EX1
SELECT INSTR('Data & A.I.', 'I')
FROM DUAL;

--SELECT INSTR('CORPORATE FLOOR','OR', 3, 2) "Instring"
--FROM DUAL;

--EX2
SELECT CONCAT(SUBSTR(FIRST_NAME, 1, 1), LAST_NAME) AS Name, SALARY, DEPARTMENT_ID
FROM EMPLOYEES
WHERE DEPARTMENT_ID = 20;

--EX3
SELECT LAST_NAME, SALARY "Salary before raise", TRUNC(SALARY * 1.05333, 2) "Salary after raise"
FROM EMPLOYEES;

--EX4
SELECT ROUND(SYSDATE, 'Month') as Month,
       ROUND(SYSDATE, 'Year') as Year,
       TRUNC(SYSDATE, 'Month') as Month,
       TRUNC(SYSDATE, 'Year') as Year
FROM DUAL;

--EX5
SELECT TO_CHAR(NEXT_DAY('25-Dec-2020','Friday'), 'fmMonth ddth, YYYY') as "First Friday"
FROM DUAL;

--EX6
SELECT FIRST_NAME, LAST_NAME, CONCAT('€', SALARY) "Current Salary", CONCAT('€',(SALARY+2000)) "New Salary"
FROM EMPLOYEES
WHERE FIRST_NAME = 'Ellen' AND
      LAST_NAME = 'Abel';

--EX7
SELECT LAST_NAME, NVL(OVERTIME_RATE, 0)
FROM F_STAFFS;

--EX8
SELECT DEPARTMENT_ID, LAST_NAME, SALARY,
       CASE department_id
           WHEN  10 THEN 1.25 * SALARY
           WHEN  90 THEN 1.5 * SALARY
           WHEN 130 THEN 1.75 * SALARY
           ELSE SALARY
           END AS "New Salary"
FROM EMPLOYEES;


SELECT t.SONG_ID, cd.TITLE
FROM D_TRACK_LISTINGS t JOIN D_CDS cd
    ON t.CD_NUMBER = cd.CD_NUMBER;

SELECT e.LAST_NAME LastName, e.SALARY, j.GRADE_LEVEL
FROM EMPLOYEES e JOIN JOB_GRADES j
    ON (SALARY BETWEEN LOWEST_SAL AND HIGHEST_SAL);

SELECT E.LAST_NAME, d.DEPARTMENT_ID, D.DEPARTMENT_NAME
FROM EMPLOYEES E LEFT JOIN DEPARTMENTS D on E.MANAGER_ID = D.MANAGER_ID;

SELECT e.last_name, e.department_id, d.department_name
FROM employees e LEFT OUTER JOIN departments d
                                 ON (e.department_id = d.department_id);

SELECT ROUND(AVG(COST), 2)
FROM D_EVENTS;

SELECT MAX(HIRE_DATE)
FROM EMPLOYEES;

SELECT EMPLOYEE_ID, HIRE_DATE, JOB_ID, DEPARTMENT_ID
FROM EMPLOYEES
UNION
SELECT EMPLOYEE_ID, START_DATE, JOB_ID, DEPARTMENT_ID
FROM JOB_HISTORY;


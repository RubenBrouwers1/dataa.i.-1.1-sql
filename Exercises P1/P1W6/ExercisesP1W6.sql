-- Week 6 semester 1

--EX1
SELECT t.song_id, c.title
FROM D_TRACK_LISTINGS t natural join D_CDS C;

SELECT t.song_id, c.title
FROM D_TRACK_LISTINGS t join D_CDS C
ON (t.CD_NUMBER = c.CD_NUMBER);

--EX2
SELECT e.LAST_NAME "Last name", e.SALARY "Salary", j.GRADE_LEVEL "Grade"
FROM EMPLOYEES e JOIN JOB_GRADES j
    ON (e.SALARY BETWEEN j.LOWEST_SAL AND j.HIGHEST_SAL);

--EX3
SELECT E.LAST_NAME, D.DEPARTMENT_ID, D.DEPARTMENT_NAME
FROM EMPLOYEES E LEFT OUTER JOIN DEPARTMENTS D
    ON E.DEPARTMENT_ID = D.DEPARTMENT_ID;

--SELECT LAST_NAME, DEPARTMENT_ID
--FROM EMPLOYEES
--WHERE DEPARTMENT_ID IS NULL;

--EX4
SELECT AVG(ROUND(COST, 2))
FROM D_EVENTS;

--EX5
SELECT (e.FIRST_NAME|| ' ' || e.LAST_NAME) Name, e.HIRE_DATE
FROM EMPLOYEES e
ORDER BY HIRE_DATE DESC
FETCH FIRST 1 ROW ONLY; --Not limit in Oracle

--EX8
SELECT EMPLOYEE_ID, JOB_ID, HIRE_DATE, DEPARTMENT_ID
FROM EMPLOYEES
UNION
SELECT EMPLOYEE_ID, JOB_ID, START_DATE, DEPARTMENT_ID
FROM JOB_HISTORY;

SELECT EMPLOYEE_ID, JOB_ID --, NVL(SALARY, 0)
FROM EMPLOYEES
UNION
SELECT EMPLOYEE_ID, JOB_ID
FROM JOB_HISTORY;

--This query will not work because there is no 'SALARY' colomn in the 'JOB_HISTORY' table.

--ROLLUP
SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY ROLLUP (department_id, job_id);

SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY (department_id, job_id);
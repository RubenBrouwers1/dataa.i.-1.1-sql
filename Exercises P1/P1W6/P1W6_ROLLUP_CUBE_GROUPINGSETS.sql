--ROLLUP
SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY ROLLUP (department_id, job_id);

SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY (department_id, job_id);

--CUBE
SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY CUBE (department_id, job_id);

--Look at the pdf for further information

--GROUPING SETS
SELECT department_id, job_id, manager_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY GROUPING SETS
    ((job_id, manager_id),(department_id, job_id),
    (department_id, manager_id))



SELECT prod_cat, MIN (prod_price)
FROM products
GROUP BY prod_price;

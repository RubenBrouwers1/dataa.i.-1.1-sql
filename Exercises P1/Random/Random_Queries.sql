select instr('Data & A.I.', 'I') as Position
from dual;

Select CONCAT(SUBSTR(FIRST_NAME, 1, 1), LAST_NAME) as Name, SALARY, DEPARTMENT_ID
from EMPLOYEES
where DEPARTMENT_ID = 20;

Select LAST_NAME, TRUNC(SALARY*1.05333, 2) as Salary
from EMPLOYEES
where DEPARTMENT_ID = 80;

SELECT ROUND(sysdate, 'MONTH') AS Month,
       ROUND(sysdate, 'YEAR') AS Year,
       TRUNC(sysdate, 'MONTH') AS Month,
       TRUNC(sysdate, 'YEAR') AS Year
FROM DUAL;

--Look at the to_char part of the query
SELECT TO_CHAR(NEXT_DAY('25-Dec-2020','Friday'), 'fmMonth ddth, YYYY') as "First Friday"
FROM DUAL;

--Finish exercises at home

Select '***'|| first_name||'***'||first_name||'***' as "Super Star"
from f_staffs;

SELECT title, year
FROM d_cds
WHERE year != 2000;

SELECT EMPLOYEE_ID AS "Number", FIRST_NAME, LAST_NAME
FROM EMPLOYEES
order by "Number" ASC;

select * from EMPLOYEES;

SELECT FIRST_NAME, LAST_NAME
FROM EMPLOYEES
Where SALARY < '8000'
  AND LAST_NAME like '%en%'
  AND HIRE_DATE > '1998-5' < '1999-6';

SELECT LAST_NAME AS "EMPLOYEE LAST NAME", SALARY AS "CURRENT SALARY", (SALARY*1.05) AS "SALARY WITH 5% RAISE"
FROM F_STAFFS;

SELECT FIRST_NAME, LAST_NAME
FROM D_PARTNERS
Where AUTH_EXPENSE_AMT = 'Not Authorized';

SELECT DEPARTMENT_ID, LAST_NAME, MANAGER_ID
FROM EMPLOYEES
WHERE EMPLOYEE_ID < 125
ORDER BY DEPARTMENT_ID  DESC;

--This piece of code does not work, it gives no results
SELECT FIRST_NAME, LAST_NAME, EMAIL
FROM EMPLOYEES
WHERE HIRE_DATE > '1996-01-01'
 AND SALARY > (9000*12)
 AND COMMISSION_PCT IS NULL;

SELECT *
FROM EMPLOYEES;

SELECT ORDER_DATE, LPAD(ORDER_TOTAL, 10, '$')
FROM F_ORDERS;

SELECT ROUND(845.553, 1), ROUND(30695.348, 2), ROUND(30695.348, -2), TRUNC(2.3454, 1)
FROM DUAL;

SELECT MOD(34,8)
FROM DUAL;

SELECT ROUND(MONTHS_BETWEEN(HIRE_DATE, sysdate), 'YEAR')
FROM --What table has to come here
WHERE FIRST_NAME = 'Bob' AND LAST_NAME = 'Miller';

SELECT LAST_DAY(SYSDATE) "Deadline"
FROM DUAL;

--What is wrong with this code?
SELECT TO_DATE('2021-01-03', 'DD-Mon-YYYY') --I know something is wrong here, but whay
FROM DUAL;

--This string also does not work
SELECT TO_DATE('JUNE192004', 'MONDDYYY') FROM DUAL;

SELECT FIRST_NAME || ' ' || LAST_NAME AS "Name", NVL(MANAGER_ID, '9999')
FROM F_STAFFS;

SELECT SYSDATE FROM DUAL;

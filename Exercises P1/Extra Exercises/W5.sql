SELECT FIRST_NAME, LAST_NAME, EMAIL
FROM EMPLOYEES
WHERE JOB_ID = 'ST_CLERK';

SELECT L.LOCATION_ID, L.CITY, D.DEPARTMENT_NAME
FROM COUNTRIES C
    JOIN LOCATIONS L ON C.COUNTRY_ID = L.COUNTRY_ID
    JOIN DEPARTMENTS D ON L.LOCATION_ID = D.LOCATION_ID
WHERE C.COUNTRY_NAME = 'Canada';

SELECT C.FIRST_NAME, C.LAST_NAME, E.EVENT_DATE, E.DESCRIPTION
FROM D_CLIENTS C LEFT OUTER JOIN D_EVENTS E ON C.CLIENT_NUMBER = E.CLIENT_NUMBER;

SELECT E.FIRST_NAME || ' ' || E.LAST_NAME AS "Employee",
       D.DEPARTMENT_NAME AS "Department",
       M.FIRST_NAME || ' ' || M.LAST_NAME AS "Manager"
FROM EMPLOYEES E
    JOIN EMPLOYEES M ON E.MANAGER_ID=M.EMPLOYEE_ID
    JOIN DEPARTMENTS D on D.DEPARTMENT_ID = E.DEPARTMENT_ID
ORDER BY DEPARTMENT_NAME;